<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access_group extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('logged_in')) redirect('login');
		if($this->session->userdata('user_level') != 2) redirect('dashboard');
        if (!$this->session->userdata('logged_in'))
            redirect('login');
        $this->cname = 'access_master';
        $this->load->model('access_group_detil_model');
        $res = $this->access_group_detil_model->getbynama($this->session->userdata('user_level'), $this->cname . '_manage');
        if ($res == 0)
            redirect('dashboard');
		$this->load->library('pagination');
		// $this->lang->load('common', $this->session->userdata('user_language'));
		// $this->lang->load('access_master', $this->session->userdata('user_language'));
	}
	
	public function index(){
		
		
		$data['css_files'] = array(base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'),
			base_url('plugins/datatables-buttons/css/buttons.bootstrap4.min.css'),
			base_url('plugins/datatables-colreorder/css/colReorder.bootstrap4.min.css'),
            base_url('plugins/datatables-keytable/css/keyTable.bootstrap4.min.css'),
            base_url('plugins/datatables-fixedheader/css/fixedHeader.bootstrap4.min.css'),
            base_url('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')
        );
        $data['js_files'] = array(base_url('plugins/datatables/jquery.dataTables.min.js'),
            base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'),
            base_url('plugins/datatables-buttons/js/dataTables.buttons.min.js'),
            base_url('plugins/datatables-buttons/js/buttons.bootstrap4.min.js'),
            base_url('plugins/datatables-buttons/js/buttons.flash.min.js'),
            base_url('plugins/datatables-buttons/js/jszip.min.js'),
            base_url('plugins/datatables-buttons/js/pdfmake.min.js'),
            base_url('plugins/datatables-buttons/js/vfs_fonts.js'),
            base_url('plugins/datatables-buttons/js/buttons.html5.min.js'),
            base_url('plugins/datatables-buttons/js/buttons.print.min.js'),
            base_url('plugins/datatables-buttons/js/buttons.colVis.min.js'),
            base_url('plugins/datatables-colreorder/js/dataTables.colReorder.min.js'),
            base_url('plugins/datatables-keytable/js/dataTables.keyTable.min.js'),
            base_url('plugins/datatables-fixedheader/js/dataTables.fixedHeader.min.js'),
            base_url('plugins/datatables-fixedcolumns/js/dataTables.fixedColumns.min.js'),
            base_url('plugins/datatables-responsive/js/dataTables.responsive.min.js'),
            base_url('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')
        );
		
		//options view
		$data['viewoptions']['type'] = "table";
		$data['viewoptions']['section'] = "management";
		$data['viewoptions']['page'] = "access_group";
		$data['viewoptions']['pageinfo'] = "Gccess Group";
		$data['viewoptions']['content'] = "management/access_group_table";
		// $data['viewoptions']['script'] = "dashboard/dashboard_admin_script";

		$this->writehtml1($data);
	}

	function access_group_datatable() 
	{
		$this->load->model('access_group_model');
		$search = $_GET['search']['value']; // Ambil data yang di ketik user pada textbox pencarian

		$columns = $this->input->get('columns');
		$search_column = array(
			"id" => (isset($columns[0]['search']['value'])) ? $columns[0]['search']['value'] : "",
			"nama" => (isset($columns[1]['search']['value'])) ? $columns[1]['search']['value'] : "",
			"keterangan" => (isset($columns[2]['search']['value'])) ? $columns[2]['search']['value'] : "",
			"updated" => (isset($columns[3]['search']['value'])) ? $columns[3]['search']['value'] : ""
		);
		
	    $limit = $this->input->get('length'); // Ambil data limit per page
	    $start = $_GET['start']; // Ambil data start
	    $order_index = $_GET['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
	    $order_field = $_GET['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
	    $order_ascdesc = $_GET['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
	    $sql_total = $this->access_group_model->record_count(); // Panggil fungsi count_all pada SiswaModel
	    $sql_data = $this->access_group_model->filter($search, $search_column, $limit, $start, $order_field, $order_ascdesc);
	    $data = array();
	    // foreach record.
	    if ($sql_data != 0) {
	    	foreach ($sql_data as $key => $value) {
				$row = array();
				$action = '<a class="btn btn-info btn-xd" href="'.site_url('access_group/updates').'/'.base64_encode($value['id']).'"><i class="fas fa-pencil-alt"></i></a>';
				$action .=' ';
				$action .= '<a class="btn btn-danger btn-xd" href="'.site_url('access_group/delete').'/'.base64_encode($value['id']).'"><i class="fas fa-trash"></i></a>';
				
				
				$row[] = $value['id'];
				$row[] = $value['nama'];
				$row[] = $value['keterangan'];
				$row[] = $value['updated'];
				$row[] = $action;

				$data[] = $row;
			}
	    }
		
		$sql_filter = $this->access_group_model->count_filter($search, $search_column); // Panggil fungsi count_filter pada SiswaModel
	    $callback = array(
	        'draw'=>$_GET['draw'], // Ini dari datatablenya
	        'recordsTotal'=>$sql_total,
	        'recordsFiltered'=>$sql_filter,
	        'data'=>$data
	    );
	    header('Content-Type: application/json');
	    echo json_encode($callback); // Convert array $callback ke json
	}
	
	public function adds($error = NULL, $detil = NULL)
	{

		$this->load->model('access_master_model');
		$data['access_master'] = $this->access_master_model->getall();
		$data['css_files'] = array(base_url('css/parsley/parsley.css'),
			base_url('plugins/select2/css/select2.min.css'),
			base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'),
			base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'),
			base_url('plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css'),
			base_url('semantic/components/statistic.min.css'));
		$data['js_files'] = array(base_url('js/knockout.js'),
			base_url('plugins/select2/js/select2.full.min.js'),
			base_url('js/parsley/parsley.min.js'),
			base_url('js/tinymce/tinymce.min.js'),
			base_url('plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js'),
			base_url('plugins/bootstrap-switch/js/bootstrap-switch.min.js'),
			base_url('js/tinymce/jquery.tinymce.min.js'));
		$this->load->helper('plugins_helper');

		//options view
		$data['viewoptions']['action'] = "add";
		$data['viewoptions']['type'] = "table";
		$data['viewoptions']['section'] = "management";
		$data['viewoptions']['page'] = "access_master";
		$data['viewoptions']['pageinfo'] = "Add Access Group";
		$data['viewoptions']['content'] = "management/access_group";
		$data['viewoptions']['script'] = "management/access_group_script";	

		$this->writehtml1($data);
	}

	public function add()
	{
		if (!isset($_POST['summary'])) redirect('error_badrequest');

		$all = json_decode($this->input->post('summary'));
		$akses = $this->input->post('akses');
		
		// check isset value
		if (!isset($all->selectedService)) $all->keterangan = '';
		if (!isset($all->nama)) $all->nama = '';

		$data = array(
			"nama" => $all->nama,
			"keterangan" => $all->keterangan,
			"created" => date('Y-m-d H:i:s'),
            "created_id" => $this->session->userdata('user_id'),
            "updated_id" => $this->session->userdata('user_id'),
		);

		//check validasi
		$this->form_validation->set_data($data);
		$this->form_validation->set_rules('nama', 'nama', 'required|trim|max_length[100]');
		$this->form_validation->set_rules('keterangan', 'keterangan ', 'trim|max_length[200]');

		if ($this->form_validation->run() == FALSE) {
			$detil[0] = $data;
			$this->adds(validation_errors(), $detil);
		}
		else {
			$this->load->model('access_group_model');
			$this->load->model('access_group_detil_model');
			$data_insert = $data;
			$insert = $this->access_group_model->add($data_insert);
			if ($insert) {
				foreach ($akses as $key => $value) {
					$data_akses = array(
						"id_access_group" => $insert,
						"id_access_master" => $value
					);
					$this->access_group_detil_model->add($data_akses);
				}
			}
			redirect('access_group');
		}
	}

	public function ajax_save() {
    
    	$this->load->model('access_group_model');
    	$akses = $this->input->post('akses');
    	$data = array(
			"nama" => $this->input->post('nama'),
			"keterangan" => $this->input->post('keterangan'),
			"created" => date('Y-m-d H:i:s'),
            "created_id" => 5,
            "updated_id" => 5,
		);
    	$data_insert = $data;
		$insert = $this->access_group_model->add($data_insert);   
		if ($akses != NULL) {
			foreach ($akses as $key => $value) {
				$data_akses = array(
					"id_access_group" => $insert,
					"id_access_master" => $value
				);
				$this->access_group_detil_model->add($data_akses);
			}
		}
		
		if($insert){
		    echo json_encode(array('status'=>true));
		}else{
		   echo json_encode(array('status'=>false));
		}
    }

    public function updates($id = NULL, $error = NULL, $detil = NULL)
	{
		
		$id = base64_decode($id);
		$this->load->model('access_master_model');
		$this->load->model('access_group_model');
		$this->load->model('access_group_detil_model');
		$res = $this->access_group_model->get($id);
		if ($res == 0) redirect('error_badrequest');

		if (!is_null($error) && !is_null($detil)) {
			$data['error'] = $error;
			$data['detil'] = $detil;
		}
		else {
			$data['detil'] = $res;
		}
		$detil_akses = $this->access_group_detil_model->getbyaccessgroupmaster($id);
		$access_master = $this->access_master_model->getallmaster();
		if ($detil_akses != 0) {
			$array_not_in =  $this->access_master_model->getallnotin($detil_akses);
		}else{
			$array_not_in = array();
		}
		

		// $array_count =  array_count_values($data_merge);
		
		$data['access_master'] = $this->access_master_model->getall();
		$data['primary'] = $id;
		$data['detil_akses'] = $this->access_group_detil_model->getbyaccessgroup($id);
		$data['array_not_in'] = $array_not_in;
		$data['css_files'] = array(base_url('css/parsley/parsley.css'),
			base_url('plugins/select2/css/select2.min.css'),
			base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'),
			base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'),
			base_url('plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css'),
			base_url('semantic/components/statistic.min.css'));
		$data['js_files'] = array(base_url('js/knockout.js'),
			base_url('plugins/select2/js/select2.full.min.js'),
			base_url('js/parsley/parsley.min.js'),
			base_url('js/tinymce/tinymce.min.js'),
			base_url('plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js'),
			base_url('plugins/bootstrap-switch/js/bootstrap-switch.min.js'),
			base_url('js/tinymce/jquery.tinymce.min.js'));
		$this->load->helper('plugins_helper');


		//options view
		$data['viewoptions']['action'] = "update";
		$data['viewoptions']['type'] = "table";
		$data['viewoptions']['section'] = "management";
		$data['viewoptions']['page'] = "access_group";
		$data['viewoptions']['pageinfo'] = "Update Access Group";
		$data['viewoptions']['content'] = "management/access_group";
		$data['viewoptions']['script'] = "management/access_group_script";

		$this->writehtml1($data);
	}

	public function update($id = NULL) {
		$id = base64_decode($id);
		
		if (!isset($_POST['summary'])) redirect('error_badrequest');

		$all = json_decode($this->input->post('summary'));
		$akses = $this->input->post('akses');
		// check isset value
		if (!isset($all->nama)) $all->nama = '';
		if (!isset($all->priority)) $all->priority = '';
		if (!isset($all->keterangan)) $all->keterangan = '';
		if (!isset($all->status)) $all->status = '';

		$data = array(
			"id" => $id,
			"nama" => $all->nama,
			"keterangan" => $all->keterangan,
            "updated_id" => $this->session->userdata('user_id'),
		);

		//check validasi
		$this->form_validation->set_data($data);
		$this->form_validation->set_rules('nama', 'nama', 'required|trim|max_length[100]');
		$this->form_validation->set_rules('keterangan', 'keterangan ', 'trim|max_length[200]');

		if ($this->form_validation->run() == FALSE) {
			$detil[0] = $data;
			$this->updates(base64_encode($id), validation_errors(), $detil);
		}
		else {
			$this->load->model('access_group_model');
			$this->load->model('access_group_detil_model');
			$data_update = $data;
			$update = $this->access_group_model->update($data_update);
			$delete = $this->access_group_detil_model->delete($id);

			foreach ($akses as $key => $value) {
				$data_akses = array(
					"id_access_group" => $id,
					"id_access_master" => $value
				);
				$this->access_group_detil_model->add($data_akses);
			}
			

			redirect('access_group');
		}
	}

	public function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();
       
        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

	private function writehtml($data){
		//load view
		$data['loginas'] = $this->session->userdata('user_name');
		$this->load->view('header', $data);
		$this->load->view('sidebar-left', $data);
		$this->load->view('navbar', $data);
		$this->load->view($data['viewoptions']['content'], $data);
		$this->load->view('footer', $data);
		if (isset($data['viewoptions']['script']))
			$this->load->view($data['viewoptions']['script'], $data);
	}
	
	private function writehtml1($data){
		//load view
		$data['loginas'] = $this->session->userdata('user_name');
		$this->load->view('header', $data);
		$this->load->view('sidebar-left', $data);
		$this->load->view('navbar', $data);
		if (isset($data['viewoptions']['content']))
			$this->load->view($data['viewoptions']['content'], $data);
		$this->load->view('footer', $data);
		if (isset($data['viewoptions']['script']))
			$this->load->view($data['viewoptions']['script'], $data);
	}
}
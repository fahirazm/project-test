<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
		parent::__construct();
		//if($this->session->userdata('logged_in')) redirect('dashboard');
		if($this->session->userdata('user_type') == 'karyawan') redirect('dashboard');
	}

	public function index()
	{

		$this->load->view('login');
	}
}

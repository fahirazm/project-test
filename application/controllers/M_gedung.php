<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_gedung extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('logged_in')) redirect('login');
		if($this->session->userdata('user_level') != 2) redirect('dashboard');
        if (!$this->session->userdata('logged_in'))
            redirect('login');
        $this->cname = 'm_gedung';
        $this->load->model('access_group_detil_model');
        $res = $this->access_group_detil_model->getbynama($this->session->userdata('user_level'), $this->cname . '_manage');
        if ($res == 0)
            redirect('dashboard');
		$this->load->library('pagination');
		// $this->lang->load('common', $this->session->userdata('user_language'));
		// $this->lang->load('m_gedung', $this->session->userdata('user_language'));
	}
	
	public function index(){
		
		
		$data['css_files'] = array(base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'),
			base_url('plugins/datatables-buttons/css/buttons.bootstrap4.min.css'),
			base_url('plugins/datatables-colreorder/css/colReorder.bootstrap4.min.css'),
            base_url('plugins/datatables-keytable/css/keyTable.bootstrap4.min.css'),
            base_url('plugins/datatables-fixedheader/css/fixedHeader.bootstrap4.min.css'),
            base_url('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')
        );
        $data['js_files'] = array(base_url('plugins/datatables/jquery.dataTables.min.js'),
            base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'),
            base_url('plugins/datatables-buttons/js/dataTables.buttons.min.js'),
            base_url('plugins/datatables-buttons/js/buttons.bootstrap4.min.js'),
            base_url('plugins/datatables-buttons/js/buttons.flash.min.js'),
            base_url('plugins/datatables-buttons/js/jszip.min.js'),
            base_url('plugins/datatables-buttons/js/pdfmake.min.js'),
            base_url('plugins/datatables-buttons/js/vfs_fonts.js'),
            base_url('plugins/datatables-buttons/js/buttons.html5.min.js'),
            base_url('plugins/datatables-buttons/js/buttons.print.min.js'),
            base_url('plugins/datatables-buttons/js/buttons.colVis.min.js'),
            base_url('plugins/datatables-colreorder/js/dataTables.colReorder.min.js'),
            base_url('plugins/datatables-keytable/js/dataTables.keyTable.min.js'),
            base_url('plugins/datatables-fixedheader/js/dataTables.fixedHeader.min.js'),
            base_url('plugins/datatables-fixedcolumns/js/dataTables.fixedColumns.min.js'),
            base_url('plugins/datatables-responsive/js/dataTables.responsive.min.js'),
            base_url('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')
        );
		
		//options view
		$data['viewoptions']['type'] = "table";
		$data['viewoptions']['section'] = "master_data";
		$data['viewoptions']['page'] = "m_gedung";
		$data['viewoptions']['pageinfo'] = "Gedung";
		$data['viewoptions']['content'] = "master_data/m_gedung_table";
		// $data['viewoptions']['script'] = "dashboard/dashboard_admin_script";

		$this->writehtml1($data);
	}

	function m_gedung_datatable() 
	{
		$this->load->model('m_gedung_model');
		$search = $_GET['search']['value']; // Ambil data yang di ketik user pada textbox pencarian

		$columns = $this->input->get('columns');
		$search_column = array(
			"id" => (isset($columns[0]['search']['value'])) ? $columns[0]['search']['value'] : "",
			"nama" => (isset($columns[1]['search']['value'])) ? $columns[1]['search']['value'] : "",
			"keterangan" => (isset($columns[2]['search']['value'])) ? $columns[2]['search']['value'] : "",
			"updated" => (isset($columns[3]['search']['value'])) ? $columns[3]['search']['value'] : "",
			"status" => (isset($columns[4]['search']['value'])) ? $columns[4]['search']['value'] : ""
		);
		
	    $limit = $this->input->get('length'); // Ambil data limit per page
	    $start = $_GET['start']; // Ambil data start
	    $order_index = $_GET['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
	    $order_field = $_GET['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
	    $order_ascdesc = $_GET['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
	    $sql_total = $this->m_gedung_model->record_count(); // Panggil fungsi count_all pada SiswaModel
	    $sql_data = $this->m_gedung_model->filter($search, $search_column, $limit, $start, $order_field, $order_ascdesc);
	    $data = array();
	    // foreach record.
	    if ($sql_data != 0) {
	    	foreach ($sql_data as $key => $value) {
				$row = array();
				$action = '<a class="btn btn-info btn-xd" href="'.site_url('m_gedung/updates').'/'.base64_encode($value['id']).'"><i class="fas fa-pencil-alt"></i></a>';
				$action .=' ';
				$action .= '<a class="btn btn-danger btn-xd" href="'.site_url('m_gedung/delete').'/'.base64_encode($value['id']).'"><i class="fas fa-trash"></i></a>';
				
				if ($value['status'] == 0) 
					$status = '<span class="badge badge-danger">Not Active</span>';
				elseif ($value['status'] == 1) 
					$status = '<span class="badge badge-success">Active</span>';
				
				
				$row[] = $value['id'];
				$row[] = $value['nama'];
				$row[] = $value['keterangan'];
				$row[] = $value['updated'];
				$row[] = $status;
				$row[] = $action;

				$data[] = $row;
			}
	    }
		
		$sql_filter = $this->m_gedung_model->count_filter($search, $search_column); // Panggil fungsi count_filter pada SiswaModel
	    $callback = array(
	        'draw'=>$_GET['draw'], // Ini dari datatablenya
	        'recordsTotal'=>$sql_total,
	        'recordsFiltered'=>$sql_filter,
	        'data'=>$data
	    );
	    header('Content-Type: application/json');
	    echo json_encode($callback); // Convert array $callback ke json
	}
	
	public function adds($error = NULL, $detil = NULL)
	{

		$data['css_files'] = array(base_url('css/parsley/parsley.css'),
			base_url('plugins/select2/css/select2.min.css'),
			base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'),
			base_url('semantic/components/statistic.min.css'));
		$data['js_files'] = array(base_url('js/knockout.js'),
			base_url('plugins/select2/js/select2.full.min.js'),
			base_url('js/parsley/parsley.min.js'),
			base_url('js/tinymce/tinymce.min.js'),
			base_url('js/tinymce/jquery.tinymce.min.js'));
		$this->load->helper('plugins_helper');

		//options view
		$data['viewoptions']['action'] = "add";
		$data['viewoptions']['type'] = "table";
		$data['viewoptions']['section'] = "master_data";
		$data['viewoptions']['page'] = "m_gedung";
		$data['viewoptions']['pageinfo'] = "Add Gedung";
		$data['viewoptions']['content'] = "master_data/m_gedung";
		$data['viewoptions']['script'] = "master_data/m_gedung_script";	

		$this->writehtml1($data);
	}

	public function add()
	{
		if (!isset($_POST['summary'])) redirect('error_badrequest');

		$all = json_decode($this->input->post('summary'));

		// check isset value
		if (!isset($all->selectedService)) $all->selectedService = '';
		if (!isset($all->nama)) $all->nama = '';
		if (!isset($all->value)) $all->value = '';

		$data = array(
			"nama" => $all->nama,
			"keterangan" => $all->keterangan,
			"status" => $all->status,
			"created" => date('Y-m-d H:i:s'),
            "created_id" => $this->session->userdata('user_id'),
            "updated_id" => $this->session->userdata('user_id'),
		);

		//check validasi
		$this->form_validation->set_data($data);
		$this->form_validation->set_rules('nama', 'nama', 'required|trim|max_length[100]');
		$this->form_validation->set_rules('keterangan', 'keterangan ', 'trim|max_length[200]');
		$this->form_validation->set_rules('status', 'Status ', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$detil[0] = $data;
			$this->adds(validation_errors(), $detil);
		}
		else {
			$this->load->model('m_gedung_model');
			$data_insert = $data;
			$insert = $this->m_gedung_model->add($data_insert);

			redirect('m_gedung');
		}
	}

	public function ajax_save() {
    
    	$this->load->model('m_gedung_model');

    	$data = array(
			"nama" => $this->input->post('nama'),
			"keterangan" => $this->input->post('status'),
			"keterangan" => $this->input->post('status'),
			"created" => date('Y-m-d H:i:s'),
            "created_id" => $this->session->userdata('user_id'),
            "updated_id" => $this->session->userdata('user_id'),
		);
    	$data_insert = $data;
		$insert = $this->m_gedung_model->add($data_insert);   
		if($insert){
		    echo json_encode(array('status'=>true));
		}else{
		   echo json_encode(array('status'=>false));
		}
    }

    public function updates($id = NULL, $error = NULL, $detil = NULL)
	{

		$id = base64_decode($id);
		$this->load->model('m_gedung_model');
		$res = $this->m_gedung_model->get($id);
		if ($res == 0) redirect('error_badrequest');

		if (!is_null($error) && !is_null($detil)) {
			$data['error'] = $error;
			$data['detil'] = $detil;
		}
		else {
			$data['detil'] = $res;
		}

		$data['primary'] = $id;

		$data['css_files'] = array(base_url('css/parsley/parsley.css'),
			base_url('plugins/select2/css/select2.min.css'),
			base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'),
			base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'),
			base_url('semantic/components/statistic.min.css'));
		$data['js_files'] = array(base_url('js/knockout.js'),
			base_url('plugins/select2/js/select2.full.min.js'),
			base_url('js/parsley/parsley.min.js'),
			base_url('js/tinymce/tinymce.min.js'),
			base_url('js/tinymce/jquery.tinymce.min.js'));
		$this->load->helper('plugins_helper');

		//options view
		$data['viewoptions']['action'] = "update";
		$data['viewoptions']['type'] = "table";
		$data['viewoptions']['section'] = "management";
		$data['viewoptions']['page'] = "m_gedung";
		$data['viewoptions']['pageinfo'] = "Update Gedung";
		$data['viewoptions']['content'] = "master_data/m_gedung";
		$data['viewoptions']['script'] = "master_data/m_gedung_script";

		$this->writehtml1($data);
	}

	public function update($id = NULL) {
		$id = base64_decode($id);

		if (!isset($_POST['summary'])) redirect('error_badrequest');

		$all = json_decode($this->input->post('summary'));
		
		// check isset value
		if (!isset($all->nama)) $all->nama = '';
		if (!isset($all->keterangan)) $all->keterangan = '';

		$data = array(
			"id" => $id,
			"nama" => $all->nama,
			"status" => $all->status,
			"keterangan" => $all->keterangan,
            "updated_id" => $this->session->userdata('user_id'),
		);

		//check validasi
		$this->form_validation->set_data($data);
		$this->form_validation->set_rules('nama', 'Nama ', 'trim|required|max_length[200]');
		$this->form_validation->set_rules('keterangan', 'keterangan ', 'trim|max_length[200]');
		$this->form_validation->set_rules('status', 'Status ', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$detil[0] = $data;
			$this->updates(base64_encode($id), validation_errors(), $detil);
		}
		else {
			$this->load->model('m_gedung_model');
			$data_update = $data;
			$update = $this->m_gedung_model->update($data_update);
			
			redirect('m_gedung');
		}
	}

	public function delete($id = NULL) {
		$id = base64_decode($id);
		$this->load->model('m_gedung_model');
		$delete = $this->m_gedung_model->delete($id);
		redirect('m_gedung');
	}

	private function writehtml($data){
		//load view
		$data['loginas'] = $this->session->userdata('user_name');
		$this->load->view('header', $data);
		$this->load->view('sidebar-left', $data);
		$this->load->view('navbar', $data);
		$this->load->view($data['viewoptions']['content'], $data);
		$this->load->view('footer', $data);
		if (isset($data['viewoptions']['script']))
			$this->load->view($data['viewoptions']['script'], $data);
	}
	
	private function writehtml1($data){
		//load view
		$data['loginas'] = $this->session->userdata('user_name');
		$this->load->view('header', $data);
		$this->load->view('sidebar-left', $data);
		$this->load->view('navbar', $data);
		if (isset($data['viewoptions']['content']))
			$this->load->view($data['viewoptions']['content'], $data);
		$this->load->view('footer', $data);
		if (isset($data['viewoptions']['script']))
			$this->load->view($data['viewoptions']['script'], $data);
	}
}
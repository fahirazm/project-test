<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function index()
	{
		if($this->session->userdata('logged_in')) redirect('dashboard');
		
		/*$this->load->library('recaptcha');
		$recaptcha = $this->input->post('g-recaptcha-response');
        if (!empty($recaptcha)) {*/
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username', 'min_length[4]|required');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[4]');
			$data = array();

			if ( $this->form_validation->run() !== false ) {
				$this->load->model('users_model');
				$user = $this->users_model->getbyusername($this->input->post('username'));
				if ($user) {
					//cari IP Address dari user
					/*if ( isset($_SERVER['HTTP_CLIENT_IP']) && ! empty($_SERVER['HTTP_CLIENT_IP'])) {
						$ip = $_SERVER['HTTP_CLIENT_IP'];
					}
					elseif ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && ! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
						$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
					}
					else {
						$ip = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
					}
					$ip = filter_var($ip, FILTER_VALIDATE_IP);
					$ip = ($ip === false) ? '0.0.0.0' : $ip;*/
					
					if (password_verify($this->input->post('password'), $user[0]['user_pass'])) {
						// ambil akses dari group
						$s_access = '';
						$this->load->model('access_group_detil_model');
						$this->load->model('access_master_model');
						$accesses = $this->access_group_detil_model->getbyaccessgroup($user[0]['user_level']);
						if ($accesses != 0) {
							foreach ($accesses as $key => $value) {
								$master = $this->access_master_model->get($value['id_access_master']);
								$s_access .= $master[0]['nama'].',';
							}
						}
						$this->session->set_userdata(array('logged_in' => true,
							'user_id' => $user[0]['user_id'],
							'user_name' => $user[0]['user_name'],
							'user_type' => 'karyawan',
							'user_level' => $user[0]['user_level'],
							'user_access' => $s_access,
						));
						/*$data['user_id'] = $res->user_id;
						$data['ip'] = $ip;
						$this->load->model('users_logs_model');
						$this->users_logs_model->add($data);*/
						redirect('dashboard');
					}
					else {
						/*$data['user_name'] = $this->input->post('username');
						$data['ip'] = $ip;
						$this->load->model('users_failed_attempts_model');
						$this->users_failed_attempts_model->add($data);*/
						$data['error_msg'] = "Invalid username and password!";
					}
				}
				else
					$data['error_msg'] = "Invalid username and password!";

			}
		/*}
		else
			$data['error_msg'] = "Invalid human verification!";*/
		$this->load->view('login', $data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
	
}
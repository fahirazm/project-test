<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// if(!$this->session->userdata('logged_in')) redirect('login');
		// if($this->session->userdata('user_type') != 'karyawan') redirect('login');
				
		$this->load->library('pagination');
		// $this->lang->load('common', $this->session->userdata('user_language'));
	}
		
	public function index(){
		// var_dump(5);exit();
		
		
		//options view
		$data['viewoptions']['type'] = "";
		$data['viewoptions']['section'] = "";
		$data['viewoptions']['page'] = "";
		$data['viewoptions']['pageinfo'] = "Dashboard";
		$data['viewoptions']['content'] = "dashboard/dashboard";
		// $data['viewoptions']['script'] = "dashboard/dashboard_admin_script";

		$this->writehtml($data);
	}
	
	
	
	private function writehtml($data){
		//load view
		$data['loginas'] = $this->session->userdata('user_name');
		$this->load->view('header', $data);
		$this->load->view('sidebar-left', $data);
		$this->load->view('navbar', $data);
		$this->load->view($data['viewoptions']['content'], $data);
		$this->load->view('footer', $data);
		if (isset($data['viewoptions']['script']))
			$this->load->view($data['viewoptions']['script'], $data);
	}
	
	private function writehtml1($data){
		//load view
		$data['loginas'] = $this->session->userdata('user_name');
		$this->load->view('header', $data);
		$this->load->view('sidebar-left', $data);
		$this->load->view('navbar', $data);
		if (isset($data['viewoptions']['content']))
			$this->load->view($data['viewoptions']['content'], $data);
		$this->load->view('footer', $data);
		if (isset($data['viewoptions']['script']))
			$this->load->view($data['viewoptions']['script'], $data);
	}
	
}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */

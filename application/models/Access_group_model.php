<?php
class Access_group_model extends CI_Model {
	  public function count_filter($search, $search_column){
		$this->db->select('access_group.id,access_group.nama,access_group.keterangan,access_group.updated');
	  	
	   	$this->db->from('access_group');
		if ($search != '' && $search != NULL) {
			$this->db->group_start();
			$this->db->like('access_group.id', $search);
			$this->db->or_like('access_group.nama', $search);
			$this->db->or_like('access_group.keterangan', $search);
			$this->db->or_like('access_group.updated', $search);

			$this->db->group_end();
		}
		if ($search_column['id'] != '' && $search_column['id'] != NULL) {
			$this->db->like('access_group.id', $search_column['id']);
		}
		if ($search_column['nama'] != '' && $search_column['nama'] != NULL) {
			$this->db->like('access_group.nama', $search_column['nama']);
		}
		if ($search_column['keterangan'] != '' && $search_column['keterangan'] != NULL) {
			$this->db->like('access_group.keterangan', $search_column['keterangan']);
		}
		if ($search_column['updated'] != '' && $search_column['updated'] != NULL) {
			$this->db->like('access_group.updated', $search_column['updated']);
		}

		return $this->db->get()->num_rows(); // Untuk menghitung jumlah data sesuai dengan filter pada textbox pencarian
	}

	public function filter($search, $search_column, $limit, $start, $order_field, $order_ascdesc) {
		$this->db->select('access_group.id,access_group.nama,access_group.keterangan,access_group.updated');
	  	
	   	$this->db->from('access_group');
		if ($search != '' && $search != NULL) {
			$this->db->group_start();
			$this->db->like('access_group.id', $search);
			$this->db->or_like('access_group.nama', $search);
			$this->db->or_like('access_group.keterangan', $search);
			$this->db->or_like('access_group.updated', $search);

			$this->db->group_end();
		}
		if ($search_column['id'] != '' && $search_column['id'] != NULL) {
			$this->db->like('access_group.id', $search_column['id']);
		}
		if ($search_column['nama'] != '' && $search_column['nama'] != NULL) {
			$this->db->like('access_group.nama', $search_column['nama']);
		}
		if ($search_column['keterangan'] != '' && $search_column['keterangan'] != NULL) {
			$this->db->like('access_group.keterangan', $search_column['keterangan']);
		}
		if ($search_column['updated'] != '' && $search_column['updated'] != NULL) {
			$this->db->like('access_group.updated', $search_column['updated']);
		}


	    $this->db->order_by($order_field, $order_ascdesc); // Untuk menambahkan query ORDER BY
	    $this->db->limit($limit, $start); // Untuk menambahkan query LIMIT
	   	$query = $this->db->get();

		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
    public function add($data) {
		$this->db->trans_start();
        $this->db->insert('access_group',$data);
		$insert_id = $this->db->insert_id();
		$this->db->trans_complete();
		return $insert_id;
    }
	
	public function update($data) {
		$array = array('id'=>$data['id']);
		$this->db->where($array)->update('access_group', $data);
	}
	
	public function record_count() {
        return $this->db->count_all('access_group');
    }

	public function getall($guest = 0) {
		$query = 

		$this->db->select('*');
		$this->db->from('access_group');
		if ($guest != '0'){
			$this->db->where('id !=', 1);
			$this->db->where('id !=', 2);
			$this->db->where('id !=', 3);
		}

		$query = $this->db->order_by('nama', 'asc')->get();		
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
	
	public function getallopen() {
		$query = $this->db->select('id, nama, keterangan')->order_by('nama', 'asc')->get('access_group');
		
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}

	public function get($id) {
		$query = $this->db->where('id', $id)->get('access_group', 1, 0);
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
	
	public function getnama($id) {
		$query = $this->db->where('LOWER(nama)', strtolower($id))->get('access_group', 1, 0);
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
	
	public function check_double($id) {
		$query = $this->db->where('LOWER(nama)', $id)->get('access_group');
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
}

<?php
class All_model extends CI_Model {
	
    public function add($table, $data) {
        $this->db->insert($table, $data);
    }
	
	public function update($table, $primary='id', $data) {
		$this->db->where($primary, $data[$primary])->update($table, $data);
	}
	
	public function record_count($table) {
        return $this->db->count_all($table);
    }

	public function getall($table, $order='id', $by='asc') {
		$query = $this->db->order_by($order, $by)->get($table);
		
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}

	public function get($table, $where='id', $id) {
		$query = $this->db->where($where, $id)->get($table, 1, 0);
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
}

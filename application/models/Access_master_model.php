<?php
class Access_master_model extends CI_Model {
	
    public function record_count() {
        return $this->db->count_all('access_master');
    }

    public function count_filter($search, $search_column){
		$this->db->select('access_master.id,access_master.nama,access_master.keterangan,access_master.updated');
	  	
	   	$this->db->from('access_master');
		if ($search != '' && $search != NULL) {
			$this->db->group_start();
			$this->db->like('access_master.id', $search);
			$this->db->or_like('access_master.nama', $search);
			$this->db->or_like('access_master.keterangan', $search);
			$this->db->or_like('access_master.updated', $search);

			$this->db->group_end();
		}
		if ($search_column['id'] != '' && $search_column['id'] != NULL) {
			$this->db->like('access_master.id', $search_column['id']);
		}
		if ($search_column['nama'] != '' && $search_column['nama'] != NULL) {
			$this->db->like('access_master.nama', $search_column['nama']);
		}
		if ($search_column['keterangan'] != '' && $search_column['keterangan'] != NULL) {
			$this->db->like('access_master.keterangan', $search_column['keterangan']);
		}
		if ($search_column['updated'] != '' && $search_column['updated'] != NULL) {
			$this->db->like('access_master.updated', $search_column['updated']);
		}

		return $this->db->get()->num_rows(); // Untuk menghitung jumlah data sesuai dengan filter pada textbox pencarian
	}

	public function filter($search, $search_column, $limit, $start, $order_field, $order_ascdesc) {
		$this->db->select('access_master.id,access_master.nama,access_master.keterangan,access_master.updated');
	  	
	   	$this->db->from('access_master');
		if ($search != '' && $search != NULL) {
			$this->db->group_start();
			$this->db->like('access_master.id', $search);
			$this->db->or_like('access_master.nama', $search);
			$this->db->or_like('access_master.keterangan', $search);
			$this->db->or_like('access_master.updated', $search);

			$this->db->group_end();
		}
		if ($search_column['id'] != '' && $search_column['id'] != NULL) {
			$this->db->like('access_master.id', $search_column['id']);
		}
		if ($search_column['nama'] != '' && $search_column['nama'] != NULL) {
			$this->db->like('access_master.nama', $search_column['nama']);
		}
		if ($search_column['keterangan'] != '' && $search_column['keterangan'] != NULL) {
			$this->db->like('access_master.keterangan', $search_column['keterangan']);
		}
		if ($search_column['updated'] != '' && $search_column['updated'] != NULL) {
			$this->db->like('access_master.updated', $search_column['updated']);
		}


	    $this->db->order_by($order_field, $order_ascdesc); // Untuk menambahkan query ORDER BY
	    $this->db->limit($limit, $start); // Untuk menambahkan query LIMIT
	   	$query = $this->db->get();

		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}

	public function add($data)
	{
		$this->db->trans_start();
		$this->db->insert('access_master',$data);
		$insert_id = $this->db->insert_id();
		$this->db->trans_complete();
		return $insert_id;
	}

	public function update($data)
	{
		$array = array('id'=>$data['id']);
		$this->db->where($array)->update('access_master', $data);
	}

	public function delete($id) {
		$this->db->delete('access_master', array('id' => $id));
	}

	public function getall() {
		$query = $this->db->order_by('nama', 'asc')->get('access_master');
		
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}

	public function getallmaster() {
		$query = $this->db->select('id as id_access_master')->order_by('nama', 'asc')->get('access_master');
		
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}

	public function get($id) {
		$query = $this->db->where('id', $id)->get('access_master', 1, 0);
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}

	public function getallnotin($array) {
		$ids = array();
		if ($array != 0 && count($array) > 0) {
		 	foreach ($array as $key => $value) {
	            $ids[] = $value['id_access_master'];
	        }
	    }

		$query = $this->db->select('id as id_access_master')->where_not_in('id', $ids)->get('access_master');
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
	
	public function getnama($id) {
		$query = $this->db->where('LOWER(nama)', strtolower($id))->get('access_master', 1, 0);
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
	
	public function check_double($id) {
		$query = $this->db->where('LOWER(nama)', $id)->get('access_master');
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
}

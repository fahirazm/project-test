<?php
class M_gedung_model extends CI_Model {
	
    public function record_count() {
        return $this->db->count_all('m_gedung');
    }

    public function count_filter($search, $search_column){
		$this->db->select('m_gedung.id,m_gedung.nama,m_gedung.keterangan,m_gedung.updated,m_gedung.status');
	  	
	   	$this->db->from('m_gedung');
		if ($search != '' && $search != NULL) {
			$this->db->group_start();
			$this->db->like('m_gedung.id', $search);
			$this->db->or_like('m_gedung.nama', $search);
			$this->db->or_like('m_gedung.keterangan', $search);
			$this->db->or_like('m_gedung.updated', $search);
			$this->db->or_like('m_gedung.status', $search);

			$this->db->group_end();
		}
		if ($search_column['id'] != '' && $search_column['id'] != NULL) {
			$this->db->like('m_gedung.id', $search_column['id']);
		}
		if ($search_column['nama'] != '' && $search_column['nama'] != NULL) {
			$this->db->like('m_gedung.nama', $search_column['nama']);
		}
		if ($search_column['keterangan'] != '' && $search_column['keterangan'] != NULL) {
			$this->db->like('m_gedung.keterangan', $search_column['keterangan']);
		}
		if ($search_column['updated'] != '' && $search_column['updated'] != NULL) {
			$this->db->like('m_gedung.updated', $search_column['updated']);
		}
		if ($search_column['status'] != '' && $search_column['status'] != NULL) {
			$this->db->like('m_gedung.status', $search_column['status']);
		}

		return $this->db->get()->num_rows(); // Untuk menghitung jumlah data sesuai dengan filter pada textbox pencarian
	}

	public function filter($search, $search_column, $limit, $start, $order_field, $order_ascdesc) {
		$this->db->select('m_gedung.id,m_gedung.nama,m_gedung.keterangan,m_gedung.updated,m_gedung.status');
	  	
	   	$this->db->from('m_gedung');
		if ($search != '' && $search != NULL) {
			$this->db->group_start();
			$this->db->like('m_gedung.id', $search);
			$this->db->or_like('m_gedung.nama', $search);
			$this->db->or_like('m_gedung.keterangan', $search);
			$this->db->or_like('m_gedung.updated', $search);
			$this->db->or_like('m_gedung.status', $search);

			$this->db->group_end();
		}
		if ($search_column['id'] != '' && $search_column['id'] != NULL) {
			$this->db->like('m_gedung.id', $search_column['id']);
		}
		if ($search_column['nama'] != '' && $search_column['nama'] != NULL) {
			$this->db->like('m_gedung.nama', $search_column['nama']);
		}
		if ($search_column['keterangan'] != '' && $search_column['keterangan'] != NULL) {
			$this->db->like('m_gedung.keterangan', $search_column['keterangan']);
		}
		if ($search_column['updated'] != '' && $search_column['updated'] != NULL) {
			$this->db->like('m_gedung.updated', $search_column['updated']);
		}
		if ($search_column['status'] != '' && $search_column['status'] != NULL) {
			$this->db->like('m_gedung.status', $search_column['status']);
		}


	    $this->db->order_by($order_field, $order_ascdesc); // Untuk menambahkan query ORDER BY
	    $this->db->limit($limit, $start); // Untuk menambahkan query LIMIT
	   	$query = $this->db->get();

		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}

	public function add($data)
	{
		$this->db->trans_start();
		$this->db->insert('m_gedung',$data);
		$insert_id = $this->db->insert_id();
		$this->db->trans_complete();
		return $insert_id;
	}

	public function update($data)
	{
		$array = array('id'=>$data['id']);
		$this->db->where($array)->update('m_gedung', $data);
	}

	public function delete($id) {
		$this->db->where('id',$id);
		$this->db->delete('m_gedung');
	}


	public function getall() {
		$query = $this->db->order_by('nama', 'asc')->get('m_gedung');
		
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}

	public function getallmaster() {
		$query = $this->db->select('id as id_m_gedung')->order_by('nama', 'asc')->get('m_gedung');
		
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}

	public function get($id) {
		$query = $this->db->where('id', $id)->get('m_gedung', 1, 0);
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}

	public function getallnotin($array) {
		$ids = array();
		if ($array != 0 && count($array) > 0) {
		 	foreach ($array as $key => $value) {
	            $ids[] = $value['id_m_gedung'];
	        }
	    }

		$query = $this->db->select('id as id_m_gedung')->where_not_in('id', $ids)->get('m_gedung');
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
	
	public function getnama($id) {
		$query = $this->db->where('LOWER(nama)', strtolower($id))->get('m_gedung', 1, 0);
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
	
	public function check_double($id) {
		$query = $this->db->where('LOWER(nama)', $id)->get('m_gedung');
		if ($query->num_rows() > 0)
			return $query->result_array();
		else
			return 0;
	}
}

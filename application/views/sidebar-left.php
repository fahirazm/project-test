<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?= base_url() ?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= base_url() ?>dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
            <a  href="<?php echo base_url("dashboard"); ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <!-- start menu master data -->
           <?php if (strpos($this->session->userdata('user_access'),'m_gedung_manage') !== false  ||
                strpos($this->session->userdata('user_access'),'m_ruangan_manage') !== false  ||
                strpos($this->session->userdata('user_access'),'users_manage') !== false ) { ?>
          <li class="nav-item has-treeview">
               <a href="#" class="nav-link<?php if($viewoptions['section'] == "master_data") echo " active"; ?>">
                 <i class="nav-icon fas fa-th"></i>
                <p>
                  Master Data
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <?php if (strpos($this->session->userdata('user_access'),'m_gedung_manage') !== false) : ?>
                <li class="nav-item" <?php if($viewoptions['section'] == "master_data" && $viewoptions['page'] == "m_gedung") echo " class=\"active\""; ?>>
                <a href="<?php echo base_url("m_gedung"); ?>" class="nav-link <?php if($viewoptions['section'] == "master_data" && $viewoptions['page'] == "m_gedung") echo " active";  ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Gedung</p>
                  </a>
                </li>
                <?php endif; ?>
                <?php if (strpos($this->session->userdata('user_access'),'m_ruangan_manage') !== false) : ?>
                <li class="nav-item" <?php if($viewoptions['section'] == "master_data" && $viewoptions['page'] == "m_ruangan") echo " class=\"active\""; ?>>
                 <a href="<?php echo base_url("m_ruangan"); ?>" class="nav-link <?php if($viewoptions['section'] == "master_data" && $viewoptions['page'] == "m_ruangan") echo " active";  ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Ruangan</p>
                  </a>
                </li>
                <?php endif; ?>
                <li class="nav-item">
                  <a href="../forms/editors.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Prodi</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/validation.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Konsentrasi</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/validation.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Kelompok Mata Kuliah</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/validation.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Grade Nilai</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/validation.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Tahun Angkatan</p>
                  </a>
                </li>
              </ul>
            </li>
          <?php } ?>
          <!-- end menu master Data -->
          <!-- start menu mahasiswa -->
          <li class="nav-header">MAHASISWA</li>
          <li class="nav-item">
            <a href="https://adminlte.io/docs/3.0" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>Mahasiswa</p>
            </a>
          </li>
          <!-- end menu mahasiswa -->
          <!-- start menu akademik -->
          <li class="nav-header">AKADEMIK</li>
            <!-- start submenu akademik -->
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-archive"></i>
                <p>
                  Akademik
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="../forms/general.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Tahun Akademik</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/advanced.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Matakuliah</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/editors.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Jadwal Kuliah</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/validation.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Registrasi</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/validation.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Kartu Rencana Studi</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/validation.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Kartu Hasil Studi</p>
                  </a>
                </li>
              </ul>
            </li>
            <!-- end sub menu akademik -->
            <!--start sub menu keuangan -->
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="fas fa-money-check-alt"></i>
                <p>
                  Keuangan
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="../forms/general.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Form Pembayaran</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/advanced.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Jenis Pembayaran</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/editors.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Biaya Kuliah</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="../forms/validation.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Laporan Keuangan</p>
                  </a>
                </li>
              </ul>
            </li>
            <!-- end sub menu keuangan -->
          <!-- end menu Akademik -->
          <li class="nav-header">MANAGEMENT</li>
          <?php if (strpos($this->session->userdata('user_access'),'access_master_manage') !== false  ||
          strpos($this->session->userdata('user_access'),'access_group_manage') !== false  ||
          strpos($this->session->userdata('user_access'),'users_manage') !== false ) { ?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link<?php if($viewoptions['section'] == "management") echo " active"; ?>">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <?php if (strpos($this->session->userdata('user_access'),'access_master_manage') !== false) : ?>
              <li class="nav-item" <?php if($viewoptions['section'] == "management" && $viewoptions['page'] == "access_master") echo " class=\"active\""; ?>>
                <a href="<?php echo base_url("access_master"); ?>" class="nav-link <?php if($viewoptions['section'] == "management" && $viewoptions['page'] == "access_master") echo " active";  ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Access Master </p>
                </a>
              </li>
              <?php endif; ?>
              <?php if (strpos($this->session->userdata('user_access'),'access_group_manage') !== false) : ?>
              <li class="nav-item" >
                <a href="<?php echo base_url("access_group"); ?>"  class="nav-link<?php if($viewoptions['section'] == "management" && $viewoptions['page'] == "access_group") echo "active"; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Access Group</p>
                </a>
              </li>
              <?php endif; ?>
              <?php if (strpos($this->session->userdata('user_access'),'users_manage') !== false) : ?>
              <li class="nav-item" >
                <a href="#" class="nav-link<?php if($viewoptions['section'] == "management" && $viewoptions['page'] == "users") echo "active"; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Users</p>
                </a>
              </li>
               <?php endif; ?>
            </ul>
          </li>
          <?php } ?>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
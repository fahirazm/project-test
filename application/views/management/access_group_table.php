
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DataTables</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?php echo $viewoptions['pageinfo']; ?></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <a class="btn btn-info" href="<?php echo base_url("access_group/adds"); ?>">Tambah Data</a>
                <br><br>
                <table id="tabel-data" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Keterangan</th>
                    <th>Updated</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Keterangan</th>
                    <th>Updated</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<!-- page script -->

<script>

  var tabel = null;
  function generateTable() {
    tabel.ajax.reload();
  }
  $(document).ready(function(){
    $('#tabel-data tfoot th').each(function() {
      var title = $(this).text();
      $(this).html('<input type="text" class="form-control" name="search_tabel" placeholder="Search' + title + '" />');
    });
    tabel = $('#tabel-data').DataTable({
      "processing": true,
      "serverSide": true,
      "responsive": true,
      "ordering": true, // Set true agar bisa di sorting
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "order": [[ 0, 'asc' ]], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
      "ajax":
      {
        "url": "<?php echo base_url('access_group/access_group_datatable') ?>", // URL file untuk proses select datanya
        "type": "GET"
      },
      "deferRender": true,
      "aLengthMenu": [[10, 20, 50],[ 10, 20, 50]], // Combobox Limit
        "columnDefs": [
          { "targets": [ 0 ],"searchable": true, "sortable" : true },
          { "targets": [ 1 ],"searchable": true, "sortable" : true },
          { "targets": [ 2 ],"searchable": false, "sortable" : false }
        ],
  
      "PaginationType": "bootstrap",
      dom: 'Bfrtip',
      colReorder: true,
      keys: true,
      fixedHeader: false,
      scrollX: true,
      buttons: [
        {
          extend: 'copyHtml5',
          exportOptions: {
            columns: [ ':visible' ]
          }
        },{
          extend: 'csvHtml5',
          exportOptions: {
            columns: [ ':visible' ]
          }
        },{
          extend: 'excelHtml5',
          exportOptions: {
            columns: [ ':visible' ]
          }
        },{
          extend: 'pdfHtml5',
          orientation: 'landscape',
          pageSize: 'LEGAL',
          exportOptions: {
            columns: [ ':visible' ]
          }
        },{
          extend: 'print',
          exportOptions: {
            columns: [ ':visible' ]
          }
        }, 'colvis'
      ]
    });
    tabel.columns().every( function () {
      var that = this;
      $('input', this.footer() ).on('keyup change', function () {
        if ( that.search() !== this.value ) {
          that.search(this.value).draw();
        }
      });
    });
  });

</script>

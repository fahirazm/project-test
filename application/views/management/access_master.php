
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $viewoptions['pageinfo']; ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

             <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><?php echo $viewoptions['pageinfo']; ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" class="form-horizontal" name="hotlineForm" data-parsley-validate action="<?php echo base_url("access_master"); ?>/<?php echo $viewoptions['action']; if($viewoptions['action'] == 'update') echo '/'.base64_encode($primary); ?>" onsubmit="return validateForm();" method="post">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12">

                        <div class="form-group">
                          <label for="nama" class="col-sm-2 control-label">Nama<sup class="text-info">*)</sup></label>
                          <div class="col-sm-12">
                              <input type="text" class="form-control" placeholder="Nama" id="nama" required data-bind="value: nama"  />
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
                          <div class="col-sm-12">
                            <textarea name="keterangan" class="wysihtml5 form-control" rows="9" data-bind="wysiwyg: keterangan"></textarea>
                          </div>
                        </div>
                      
                      <!-- /.card-body -->

                      <div class="card-footer">
                        <input type="hidden" name="summary" data-bind="value: ko.toJSON($root)">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <?php if($viewoptions['action'] == 'add') : ?>
                          <button type="button" id="submit_ajax" class="btn btn-success">Simpan & Kembali</button>
                        <?php endif; ?>
                        <button type="button" onclick="javascript:history.back()" class="btn btn-danger"><span ></span> Cancel</button>
                      </div>
                    </div>
                  </div>
              
              </div>
            </form>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<!-- page script -->

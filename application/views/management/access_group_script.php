<script>

	<?= get_tinymce_knockout() ?> 

	function OrganizerViewModel(){ 
		var self = this; 
		

		self.isDirty = ko.observable('false');
		self.id = ko.observable(`<?php if(isset($detil)) echo $detil[0]['id'] ?>`); 
		self.nama = ko.observable(`<?php if(isset($detil)) echo $detil[0]['nama'] ?>`); 
		self.keterangan = ko.observable(`<?php if(isset($detil)) echo $detil[0]['keterangan'] ?>`); 
		self.itsDirty = function() { self.isDirty(true); }	

		//ketika submit button d click
	    $("#submit_ajax").click(function(){
	        if (self.nama() == '') {
                alert(`Nama Belum Di isi`);
                return false;
            }
	        //do ajax proses
	        $.ajax({
	           
	           url: "<?php echo base_url("access_group"); ?>/ajax_save",
	           type: "post", //form method
	           data: {
						nama: self.nama(),
						keterangan : self.keterangan(),
						akses : $("#akses").val()
					},
	           dataType:"json", //misal kita ingin format datanya brupa json
	           beforeSend:function(){
	             //lakukan apasaja sambil menunggu proses selesai disini
	             //misal tampilkan loading
	             $("#submit_ajax").prop( "disabled", true );
				 $("#submit_ajax").html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Loading...');
	             
	           },
	           success:function(result){
	           	if(result.status){
	             	window.location.href = "<?php echo base_url("access_group"); ?>";
	              	}else{
	               		alert("harap isi semua inputan");
	             	}
	            },
	           
	           
	        });   
	       return false;
	     })
	} 

	$(document).ready(function () {
		ko.applyBindings(new OrganizerViewModel()); 
	});

	$(function() {

		$('.duallistbox').bootstrapDualListbox()

		$("body").keypress(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13) {
				return false;
			}
		});
	});

	$('#hotlineForm').parsley();

	window.Parsley.on('form:submit', function() {
		$(':submit').each(function() {
			$(this).prop( "disabled", true );
			$(this).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Loading...');
		});
	});

</script> 

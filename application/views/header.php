<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link href="<?php echo base_url("plugins/fontawesome-free/css/all.min.css"); ?>" rel="stylesheet">
  <!-- Ionicons -->
  <link href="<?php echo base_url("dist/css/ionicons/css/ionicons.min.css"); ?>" rel="stylesheet">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link href="<?php echo base_url("plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"); ?>" rel="stylesheet">
  <!-- iCheck -->
  <link href="<?php echo base_url("plugins/icheck-bootstrap/icheck-bootstrap.min.css"); ?>" rel="stylesheet">
  <!-- JQVMap -->
  <link href="<?php echo base_url("plugins/jqvmap/jqvmap.min.css"); ?>" rel="stylesheet">
  <!-- Theme style -->
  <link href="<?php echo base_url("dist/css/adminlte.min.css"); ?>" rel="stylesheet">
  <!-- overlayScrollbars -->
  <link href="<?php echo base_url("plugins/overlayScrollbars/css/OverlayScrollbars.min.css"); ?>" rel="stylesheet">
  <!-- Daterange picker -->
  <link href="<?php echo base_url("plugins/daterangepicker/daterangepicker.css"); ?>" rel="stylesheet">
  <!-- summernote -->
  <link href="<?php echo base_url("plugins/summernote/summernote-bs4.css"); ?>" rel="stylesheet">
  <!-- Google Font: Source Sans Pro -->
  <link href="<?php echo base_url("dist/css/fonts.googleapi-sans-pro.css"); ?>" rel="stylesheet">
   <!-- jQuery -->
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
  <script src="<?php echo base_url("plugins/jquery/jquery.min.js"); ?>"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?php echo base_url("plugins/jquery-ui/jquery-ui.min.js"); ?>"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <?php if (isset($css_files)) {
  foreach($css_files as $file) { ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
  <?php } } ?>
  <?php if (isset($js_files)) {
  foreach($js_files as $file){ ?>
    <script src="<?php echo $file; ?>"></script>
  <?php } } ?>
</head>
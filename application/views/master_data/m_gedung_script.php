<script>
	//binding select2
	ko.bindingHandlers.select2 = {
		after: ["options", "value"],
		init: function (el, valueAccessor, allBindingsAccessor, viewModel) {
			$(el).select2(ko.unwrap(valueAccessor()));
			ko.utils.domNodeDisposal.addDisposeCallback(el, function () {
				$(el).select2('destroy');
			});
		},
		update: function (el, valueAccessor, allBindingsAccessor, viewModel) {
			var allBindings = allBindingsAccessor();
			var select2 = $(el).data("select2");
			if ("value" in allBindings) {
				var newValue = "" + ko.unwrap(allBindings.value);
				if ((allBindings.select2.multiple || el.multiple) && newValue.constructor !== Array) {
					select2.val([newValue.split(",")]);
				}
				else {
					select2.val([newValue]);
				}
			}
		}
	};
	<?= get_tinymce_knockout() ?> 

	function OrganizerViewModel(){ 
		var self = this; 
		
		self.availableStatus = ko.observableArray([{"id":"0", "nama":"Not Active"}, {"id":"1", "nama":"Active"}]);

		self.isDirty = ko.observable('false');
		self.id = ko.observable(`<?php if(isset($detil)) echo $detil[0]['id'] ?>`); 
		self.nama = ko.observable(`<?php if(isset($detil)) echo $detil[0]['nama'] ?>`); 
		self.keterangan = ko.observable(`<?php if(isset($detil)) echo $detil[0]['keterangan'] ?>`); 
		self.status = ko.observable(`<?php if(isset($detil)) echo $detil[0]['status'] ?>`); 
		self.itsDirty = function() { self.isDirty(true); }	

		//ketika submit button d click
	    $("#submit_ajax").click(function(){
	        if (self.nama() == '') {
                alert(`Nama Belum Di isi`);
                return false;
            }
	        //do ajax proses
	        $.ajax({
	           
	           url: "<?php echo base_url("m_gedung"); ?>/ajax_save",
	           type: "post", //form method
	           data: {
						nama: self.nama(),
						status : self.status(),
						keterangan : self.keterangan()
					},
	           dataType:"json", //misal kita ingin format datanya brupa json
	           beforeSend:function(){
	             //lakukan apasaja sambil menunggu proses selesai disini
	             //misal tampilkan loading
	             $("#submit_ajax").prop( "disabled", true );
				 $("#submit_ajax").html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Loading...');
	             
	           },
	           success:function(result){
	           	if(result.status){
	             	window.location.href = "<?php echo base_url("m_gedung"); ?>";
	              	}else{
	               		alert("harap isi semua inputan");
	             	}
	            },
	           
	           
	        });   
	       return false;
	     })
	} 

	$(document).ready(function () {
		ko.applyBindings(new OrganizerViewModel()); 
	});

	$(function() {

	
		//Bootstrap Duallistbox
    	
		$("body").keypress(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13) {
				return false;
			}
		});
	});

	$('#hotlineForm').parsley();

	window.Parsley.on('form:submit', function() {
		$(':submit').each(function() {
			$(this).prop( "disabled", true );
			$(this).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Loading...');
		});
	});

</script> 

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link href="<?php echo base_url("plugins/fontawesome-free/css/all.min.css"); ?>" rel="stylesheet">
  <!-- Ionicons -->
  <link href="<?php echo base_url("dist/css/ionicons/css/ionicons.min.css"); ?>" rel="stylesheet">
  <!-- icheck bootstrap -->
  <link href="<?php echo base_url("plugins/icheck-bootstrap/icheck-bootstrap.min.css"); ?>" rel="stylesheet">
  <!-- Theme style -->
  <link href="<?php echo base_url("dist/css/adminlte.min.css"); ?>" rel="stylesheet">
  <!-- Google Font: Source Sans Pro -->
  <link href="<?php echo base_url("dist/css/fonts.googleapi-sans-pro.css"); ?>" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Admin</b>LTE</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="<?php echo base_url("auth"); ?>" method="post">
        <div class="input-group mb-3">
          <input type="text" name="username" class="form-control" placeholder="Username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fa fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <!-- /.social-auth-links -->

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url("plugins/jquery/jquery.min.js"); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url("plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("dist/js/adminlte.js"); ?>"></script>

</body>
</html>
